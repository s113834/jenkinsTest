Feature: Counter
  Remove the feature and replace with the features
  for your application.

  Scenario: incrementing a counter
    Given the initial value of the counter as 10
    When the counter is incremented
    Then the final value of the counter is 11
